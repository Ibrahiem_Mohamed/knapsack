package com.mobiquityinc.packer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.mobiquityinc.packer.exception.APIException;

/**
 * Unit test for Packer Library.
 */
public class PackerTest {

	private final static String filePath = "test.txt";

	@ClassRule
	public static TemporaryFolder folder = new TemporaryFolder();
	
	public static File tempFile;
	
	public static String tempFileContent =
			"81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)\r\n" + 
			"\r\n" + 
			"8 : (1,15.3,€34)\r\n" + 
			"\r\n" + 
			"75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)\r\n" + 
			"\r\n" + 
			"56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)\r\n" ;
	
	public static String tempFileSolution =
			"4\n" + 
			"-\n" + 
			"2,7\n" + 
			"8,9\n" ;
	
	@BeforeClass
	public static void testCreateFile() throws IOException {
		tempFile = folder.newFile(filePath);
		FileWriter writer = new FileWriter(tempFile);
		writer.write(tempFileContent);
		writer.close();
		assertTrue(tempFile.exists());
	}

	@Test(expected = APIException.class)
	public void testThowAPIException() throws APIException {
		Packer.pack("MockFakeFile.txt");
	}
	
	
	@Test
	public void testTempFileSolution() throws APIException {
		assertEquals(tempFileSolution , Packer.pack(tempFile.getAbsolutePath()));
	}

	
}
