package com.mobiquityinc.packer.model;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public final class Item implements Comparable<Item> {

	private int index;
	private double weight;
	private double cost;

	/**
	 * @param index
	 * @param weight
	 * @param cost
	 */
	public Item(int index, double weight, double cost) {
		super();
		this.index = index;
		this.weight = weight;
		this.cost = cost;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}

	@Override
	public int compareTo(Item item) {
		return this.index - item.index;

	}

}
