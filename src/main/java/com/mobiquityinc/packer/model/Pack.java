package com.mobiquityinc.packer.model;

import java.util.List;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public final class Pack {

	private double limit;
	private List<Item> items;
	
	
	/**
	 * @param limit
	 * @param items
	 */
	public Pack(double limit, List<Item> items) {
		super();
		this.limit = limit;
		this.items = items;
	}
	/**
	 * @return the limit
	 */
	public double getLimit() {
		return limit;
	}
	/**
	 * @param limit the limit to set
	 */
	public void setLimit(double limit) {
		this.limit = limit;
	}
	/**
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
}
