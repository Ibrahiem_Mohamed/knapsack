package com.mobiquityinc.packer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.mobiquityinc.packer.exception.APIException;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.model.Pack;

/**
 * Introduction You want to send your friend a package with different things.
 * Each thing you put inside the package has such parameters as index number,
 * weight and cost. The package has a weight limit. Your goal is to determine
 * which things to put into the package so that the total weight is less than or
 * equal to the package limit and the total cost is as large as possible. You
 * would prefer to send a package which weights less in case there is more than
 * one package with the same price. Input sample Your program should accept as
 * its first argument a path to a filename. The input file contains several
 * lines. Each line is one test case. Each line contains the weight that the
 * package can take (before the colon) and the list of things you need to
 * choose. Each thing is enclosed in parentheses where the 1st number is a
 * thing's index number, the 2nd is its weight and the 3rd is its cost. E.g.
 *  81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48) 
 *  8 : (1,15.3,€34) 
 *  75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
 *  56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)
 *   Output
 * sample For each set of things that you put into the package provide a list
 * (items’ index numbers are separated by comma). E.g.
 *  4
 *  -
 *  2,7
 *  8,9
 *  
 *  Proposed solution here is to create all different combinations of items and then decide which to choose comparing weight and price
 */
public final class Packer {

	/*
	 * private constructor as a static public method is used as a signature for
	 * reading and returning the solution and preventing creating an instance
	 */

	private double capacityLimit;
	private List<Item> items;
	private List<List<Item>> combinations;
	private static final double MAX_WEIGHT = 100;

	private Packer(double limit, List<Item> items) {
		this.capacityLimit = limit;
		this.items = items;
		combinations = new ArrayList<List<Item>>();
	}

	/**
	 * @param filePath
	 * @return String containing the solution of all the lines
	 * @throws APIException
	 */
	public static String pack(String filePath) throws APIException {

		// the final result string
		String solution = "";

		// 1-1 . open file and start reading
		File file = new File(filePath);
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = in.readLine()) != null) {
				if (line.length() == 0)
					continue;

				// 1-2 . parse each line to a pack object with pack limit and the items which
				// will be test
				Pack pack = parseLine(line);

				// 2. create a new object containing the items and maxLimit of the pack
				Packer p = new Packer(pack.getLimit(), pack.getItems());

				// 3. filtering items which weight is bigger than the capacity
				p.filterItems();

				// 4. create combinations of all possible items
				p.combinations = p.createCombinations();

				// 5. find the best combination of items that fulfill the requirements
				List<Item> bestCombination = p.getBestPackage();

				// 6. sorting the numbers based on the index value
				Collections.sort(bestCombination);

				// 7. Append the result of the single pack to the hole file packs
				solution += p.printOutput(bestCombination);
			}
		} catch (IOException e) {
			throw new APIException(e.getMessage());
		}

		return solution;

	}

	/**
	 * @param line
	 * @return Pack object containing the pack limit and all items to be tried 
	 */
	private static Pack parseLine(String line) {
		String[] lineArray = line.split(":");
		double capacityLimit = Integer.parseInt(lineArray[0].trim());
		String[] stringItems = lineArray[1].trim().split(" ");
		ArrayList<Item> inputs = new ArrayList<Item>();
		for (String stringItem : stringItems) {
			String[] itemDetails = stringItem.split(",");
			int index = Integer.parseInt(itemDetails[0].substring(1));
			double weight = Double.parseDouble(itemDetails[1]);
			double cost = Double.parseDouble(itemDetails[2].substring(1, itemDetails[2].length() - 1));
			Item item = new Item(index, weight, cost);
			inputs.add(item);
		}
		return new Pack(capacityLimit, inputs);
	}

	/**
	 * filter all items which it is weight is bigger than the MAX_WEIGHT
	 * 
	 */
	private void filterItems() {
		items = items.stream().filter(item -> item.getWeight() <= capacityLimit).collect(Collectors.toList());
	}

	/**
	 * @return A List contains all combinations possible 
	 */
	private List<List<Item>> createCombinations() {
		// loop through every item to make all combinations of different items
		for (int x = 0; x < items.size(); x++) {
			Item currentItem = items.get(x);
			int combinationSize = combinations.size();
			for (int y = 0; y < combinationSize; y++) {
				List<Item> combination = combinations.get(y);
				List<Item> newCombination = new ArrayList<Item>(combination);
				newCombination.add(currentItem);
				combinations.add(newCombination);
			}
			List<Item> current = new ArrayList<Item>();
			current.add(currentItem);
			combinations.add(current);
		}
		return combinations;
	}

	/**
	 * @param items
	 * @return sum of items weight
	 */
	private double getWeight(List<Item> items) {

		return items.stream().mapToDouble(Item::getWeight).sum();
	}
	
	/**
	 * @param items
	 * @return sum of items cost
	 */
	private double getPrice(List<Item> items) {
		return items.stream().mapToDouble(Item::getCost).sum();

	}

	/**
	 * @return List of best matched items to be put in the Pack
	 */
	private List<Item> getBestPackage() {
		List<Item> bestCombination = new ArrayList<Item>();
		// Initial best weight and cost 
		double bestCost = 0;
		double bestWeight = MAX_WEIGHT;
		
		for (List<Item> combination : combinations) {
			double combinationWeight = getWeight(combination);
			if (combinationWeight > capacityLimit) {
				continue;
			} else {
				double combinationPrice = getPrice(combination);
				// compare current with the best price 
				if (combinationPrice > bestCost) {
					bestCost = combinationPrice;
					bestCombination = combination;
					bestWeight = combinationWeight;
				} else if (combinationPrice == bestCost) { 
					// using the lightest weight
					if (combinationWeight < bestWeight) {
						bestCost = combinationPrice;
						bestCombination = combination;
						bestWeight = combinationWeight;
					}
				}
			}
		}
		return bestCombination;
	}

	/**
	 * @param items
	 * @return String contains the items separated by a comma
	 */
	private String printOutput(List<Item> items) {

		// case there is no matched items found to fill the pack
		if (items.isEmpty())
			return "-\n";

		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Item i : items) {
			if (isFirst) {
				sb.append(i.getIndex());
				isFirst = false;
			} else {
				sb.append("," + i.getIndex());
			}
		}

		sb.append("\n");
		return sb.toString();
	}

}
