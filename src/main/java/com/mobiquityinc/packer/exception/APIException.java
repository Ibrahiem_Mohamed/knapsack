package com.mobiquityinc.packer.exception;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public final class APIException extends Exception {

	private static final long serialVersionUID = 1L;

	public APIException(String msg) {
		super(msg);
	}

}
